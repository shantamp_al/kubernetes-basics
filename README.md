# Kubernetes 101

It is a way to manage and deploy containers at scale
Works like terraform but for containers, with some extra features.

Includes:
- Self healing
- Monitoring

### How does it work?

K8 is setup as a cluster (similiar to Ansible).
- K8 Control panel
- K8 worker nodes

Inside the nodes you have:
- Namespaces (like a vpc)
- Services (endpoints for pods)
- Pods (container + launch templates)

### Pre requisites and pre install notes

There are different ways to get a K8 cluster. It's generally a bit tricky to set up by yourself. The following are options

- Use kubespray (Ansible)
- Kubeadm

Other srvices of kubernetes that are managed

- AWS EKS
- Google GKE

Different distro of K8:

- K8 Official
- minikube
- Microk8s

### Our setup

We'll use kubespray:

[click here](https://github.com/kubernetes-sigs/kubespray)

We're going to need:

- 1 k8 control panel
- 2 k8 worker nodes
- 1 external Ansible machine with the right access - t3a.large

Minimu requirements mentions:

- 16GM RAM
- 4 CPU


### Steps

1. Start Machines 

We need 4 t3a.xlarge machines.
Start these and give them appropriate tags:

- Ansible
- K8 Control Panel
- K8 Agent A
- K8 Agent B


2. Allow SG Networking

Ansible will have to connect to K8 machines to setup.

Allow Ansible into those machines via SGs:

- `Cohort_9_home_ips`
- `AllowAllJenkinsmachines`

3. Create and share key-pair

In ansible machine, `ssh-keygen` a new key-pair.

Log into K8 machines and add the public key to the `authorized_keys` file

4. Clone Kubespray and install dependency packages

```bash
$ git clone ttps://github.com/kubernetes-sigs/kubespray.git

# Update virtual repositories
sudo apt update

# Install pip3
sudo apt install python3-pip

# Run command to install dependency packages
$ pip3 install -r requirements.txt

```

5. Edit inventory file

Kubespray has a sample inventory file we can copr and then edit with K8 machines private IPs

```bash
# Copy ``inventory/sample`` as ``inventory.mycluster``
cp -rfp inventory/sample inventory/mycluster
```

On the inventory file add your K8 private IPs and specify ssh. SSH Key can also be specifiec when calling the ansible playbook.

```YAML
##Copy of your inventory yaml from my cluster here

### Update 
```

6. Run playbook with inventory file

ansible-playbook -i inventory.ini 



