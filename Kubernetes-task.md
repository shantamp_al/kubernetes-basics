# Kubernetes and Kubectl!

### K8 Control panel

The K8 control panel is where we can define services, deployments and generally describe the desired state of these in the K8 worker nodes.

General terminology around K8 control panel:

- `kubect`
- pods
- K8 nodes
- namespaces
- deployment files
- Services

In your K8 control panel, you interact with your cluster mainly via `kubectl` - The Kubernetes CLI to make things happen.

### Starting the service

Once yu start your cluster you'll need to run:

```bash
$ export KUBECONFIG=/etc/kubernetes/admin.conf or $HOME/.kube/config
$ sudo cp /etc/kubernetes/admin.conf $HOME/
$ sudo chown $(id -u):$(id -g) $HOME/admin.conf
$ export KUBECONFIG=$HOME/admin.conf
```


### Task 1 - Namespaces

Is a sectioned area, where you can then deploy services and pods into. Like a VPC, but inside the Nodes.

```bash
# Check existing NAMESPACES
$ kubectl get ns

# Create new namespace
$ kubectl create namespace <name>

# Delete a namespace
$ kubectl delete ns <nameofnamespace>

```
### Task 2 - Deployment Files: now we need to deloy some pods (these have containers)

**Deployment &services** 

Two main sections that we will work on. They are a declaritive way to deloy pods into the cluster and create networking "services" to connect and expose these.

Q) What is a deployment file?
A) A deployment file is used to configure/specify which application modules will run in which containers

Q) What is the language used to write a Deployment file?
A) The language used is YML

Q) What "makes it a deployment file"?
A) When you specify a `kind` you name it `deployment`

Q) What are the three sections?
A) `apiVersion`, `kind` and `metadata`

Q) Why are labels important?
A) Labels are used to pair/attach objects such as pods to the deployment

Q) Where do you specify the container IMAGE to use?
A) Under `spec` `container` you use `name` and `image`

Q) How do you run your deployment file into your new Namesapce?
```bash
A) $ kubectl apply -f <nameofdeploymentfile> --namespace <nameofnamespace>
```
Q) How do your check how many pods are running?
```bash
A) $ kubectl get pods --namespace <nameofnamespace>
```

Q) How do you kill a pod?
```bash
A) $ kubectl delete pod <nameofpod>
```

Q) how do you describe a pod?
```bash
A) kubectl describe pod <nameofpod> -n <namespace>
```

Q) What extra information do you get?
A) You will have an extra collumn called `message`


Q) How do you ssh into a pod?
```bash
A) $ kubectl exec -it <nameofpod> -n nginx-test -- /bin/bash 
```
Q) How do you modify a deployment setup?
```bash
A) $ kubectl scale deployment/nginx-deployment -n nginx-test --replicas=3
```

Q) change the Image?
```bash
A) $ kubectl set image deployment/nginx-deployment nginx=nginx:1.16.1
```

Q) Change the number of pods?
```bash
A)
```





















